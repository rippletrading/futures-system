using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FuturesSystem
{
  public class Settlement
  {
    public enum SettlementType
    {
      ExpiredOTM = 1,
      ExpiredITM = 2,
      Exited = 3,
      StoppedOut = 4
    }


    public SettlementType settlementType;
    public DateTime dateCreated = DateTime.Now;
    public decimal profitLoss;
    public int id;
    public long ironCondorID;
    public decimal underlyingPrice;
    public DateTime underlyingDate;
    public int contracts;
    public decimal commission;

    public decimal risk;


    public Future future;

    public DateTime inTime = DateTime.MinValue;
    public DateTime outTime;



  }
}
