using Skender.Stock.Indicators;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuturesSystem
{
    public static class Strategies
    {


        public static Future runTwoHAEntry(IndexQuote currBar, DateTime lastExit, Dictionary<string, IndexQuote> rollingData, decimal vix)
        {
            Future f = new Future();

            List<IndexQuote> indexQuotes = rollingData.Values.ToList();

            List<HeikinAshiResult> haCandles = indexQuotes.GetHeikinAshi().ToList(); //Take(50).

            var atr = indexQuotes.GetAtr(5).Single(x => x.Date == currBar.Date.AddMinutes(-5)).Atr;
            var pSar = indexQuotes.GetParabolicSar().Single(x => x.Date == currBar.Date.AddMinutes(-5));
            var sRSI = indexQuotes.GetStochRsi(7, 7, 3, 3).Single(x => x.Date == currBar.Date.AddMinutes(-5));


            DateTime startOfDay = DateTime.Now.Date;
            startOfDay = startOfDay.AddHours(7).AddMinutes(40);

            DateTime endOfDay = DateTime.Now.Date;
            endOfDay = endOfDay.AddHours(13).AddMinutes(55);


            if (currBar.Date.DayOfWeek != DayOfWeek.Sunday && currBar.Date.DayOfWeek != DayOfWeek.Saturday && currBar.Date >= startOfDay && currBar.Date <= endOfDay) //PUT THIS BACK IN WHEN WE GET VIX
            {

                HeikinAshiResult lastBar = haCandles.Where(x => x.Date == currBar.Date.AddMinutes(-5)).SingleOrDefault();
                HeikinAshiResult twoAgoBar = haCandles.Where(x => x.Date == currBar.Date.AddMinutes(-10)).SingleOrDefault();
                IndexQuote lastBarI = indexQuotes.Where(x => x.Date == currBar.Date.AddMinutes(-5)).SingleOrDefault();


                if (lastBar != null && twoAgoBar != null && currBar.Date >= lastExit && atr > Convert.ToDecimal(1.75))
                {

                    //Console.WriteLine("runTwoHAEntry atr: " + atr + " open " + lastBar.Open + " Low " + lastBar.Low + " open " + twoAgoBar.Open + " low " + twoAgoBar.Low + " close " + lastBarI.Close);
                    //Console.WriteLine("runTwoHAEntry atr: " + atr + " open " + lastBar.Open + " high " + lastBar.High + " open " + twoAgoBar.Open + " high " + twoAgoBar.High + " close " + lastBarI.Close);


                    // if ( lastBar.Open == lastBar.Low && twoAgoBar.Open == twoAgoBar.Low && lastBarI.Close > pSar.Sar && sRSI.StochRsi > sRSI.Signal ) 
                    if (lastBar.Open == lastBar.Low && twoAgoBar.Open == twoAgoBar.Low && lastBarI.Close > pSar.Sar && sRSI.StochRsi > sRSI.Signal)
                    {

                        f.tradeType = Future.TradeType.BUY;
                        f.longOrShort = Future.LONG_OR_SHORT.LONG;
                        f.inBar = currBar;
                        f.strategyPassed = true;

                    }
                    //  else if (lastBar.Open == lastBar.High && twoAgoBar.Open == twoAgoBar.High  && lastBarI.Close < pSar.Sar && sRSI.Signal > sRSI.StochRsi )
                    else if (lastBar.Open == lastBar.High && twoAgoBar.Open == twoAgoBar.High && lastBarI.Close < pSar.Sar && sRSI.Signal > sRSI.StochRsi)
                    {

                        f.tradeType = Future.TradeType.SELL;
                        f.longOrShort = Future.LONG_OR_SHORT.SHORT;
                        f.inBar = currBar;
                        f.strategyPassed = true;

                    }
                }
            }

            return f;

        }

        public static bool exitTwoHA(IndexQuote currBar, List<HeikinAshiResult> haCandles, List<IndexQuote> indexQuotes, List<Future> activeFutures, IBWrapper wrapper)
        {
            HeikinAshiResult lastBarHA = haCandles.Where(x => x.Date == currBar.Date.AddMinutes(-5)).SingleOrDefault();
            HeikinAshiResult twoLastBarHA = haCandles.Where(x => x.Date == currBar.Date.AddMinutes(-10)).SingleOrDefault();
            IndexQuote lastIndexQuote = indexQuotes.Where(x => x.Date == currBar.Date.AddMinutes(-5)).SingleOrDefault();
            Future future = activeFutures.FirstOrDefault();

            bool exit = false;

            var pSar = indexQuotes.GetParabolicSar().Single(x => x.Date == currBar.Date.AddMinutes(-5));

            if ((currBar.Date - lastBarHA.Date).TotalMinutes > 5)
            {

                wrapper.sendAlertAsync("Exited with old data! Shouldn't happen, look into... " + currBar.Date.ToString() + " | " + lastBarHA.Date.ToString());
                Console.WriteLine("Exited with old data! Shouldn't happen, look into... " + currBar.Date.ToString() + " | " + lastBarHA.Date.ToString() + " currtime: " + DateTime.Now.ToString("hh.mm.ss.ffffff"));
            }


            if (future.longOrShort == Future.LONG_OR_SHORT.LONG && currBar.Date > future.lastPassDateTime && ((lastBarHA.Open == lastBarHA.High && lastIndexQuote.Close < pSar.Sar) || (lastBarHA.Open == lastBarHA.High && twoLastBarHA.Open == twoLastBarHA.High)))
            {

                if (currBar.Close - future.inBar.Open > 0 && !future.usedOnePass)
                {
                    future.usedOnePass = true;
                    future.lastPassDateTime = currBar.Date;
                    Console.WriteLine("Using up a pass, current pl:" + ((currBar.Close - future.inBar.Open) * 50) + " " + DateTime.Now.ToString("hh.mm.ss.ffffff"));
                }
                else
                {
                    exit = true;
                }
            }
            else if (future.longOrShort == Future.LONG_OR_SHORT.SHORT && currBar.Date > future.lastPassDateTime && ((lastBarHA.Open == lastBarHA.Low && lastIndexQuote.Close > pSar.Sar) || (lastBarHA.Open == lastBarHA.Low && twoLastBarHA.Open == twoLastBarHA.Low)))
            {
                if (currBar.Close - future.inBar.Open < 0 && !future.usedOnePass)
                {
                    future.usedOnePass = true;
                    future.lastPassDateTime = currBar.Date;
                    Console.WriteLine("Using up a pass, current pl:" + ((currBar.Close - future.inBar.Open) * 50 * -1) + " " + DateTime.Now.ToString("hh.mm.ss.ffffff"));
                }
                else
                {
                    exit = true;
                }

            }
            else if (currBar.Date.Hour == 14 && currBar.Date.Minute == 55) //no holding overnight
            {
                exit = true;
            }

            return exit;

        }

    }


}
