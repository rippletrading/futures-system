using Skender.Stock.Indicators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuturesSystem
{
    public class IndexQuote : IQuote
    {
        public DateTime Date { get; set; }
        public decimal Open { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal Close { get; set; }
        public decimal Volume { get; set; }
        public DateTime lastUpdated { get; set; }

        public IndexQuote()
        {
            lastUpdated = DateTime.Now;
        }

        public IndexQuote(decimal _open, decimal _high, decimal _low, decimal _close, DateTime _date)
        {
            Open = _open;
            High = _high;
            Low = _low;
            Close = _close;
            Date = _date;

        }

    }
}
