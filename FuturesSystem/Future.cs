using IBApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuturesSystem
{
  public class Future
  {
    public Future()
    {
      commission = 0;
    }

    public enum TradeType { BUY = 1, SELL = 2 }
    public enum LONG_OR_SHORT { LONG = 1, SHORT = 2 }

    public enum OfferType { MKT = 1, LMT = 2 }

    public DateTime tradeDate;
    public double commission;
    public long id = 0;
    public int runID;
    public dynamic indicatorJSON;
    public int contracts = 1;
    public double maxRisk = 0;
    public Settlement settlement;
    public IndexQuote inBar;
    public IndexQuote outBar;
    public TradeType tradeType;
    public decimal stop;
    public decimal profitTarget;
    public bool strategyPassed = false;
    public OfferType offerType;
    public LONG_OR_SHORT longOrShort;

    public Trade openTrade;
    public Trade closeTrade;

    public decimal runningPL;
    public decimal currContracts;

    public bool usedOnePass = false;

    public dynamic entryJson;
    public dynamic exitJson;

    public DateTime lastPassDateTime = DateTime.MinValue;



  }
}
