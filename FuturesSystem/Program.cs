using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using HtmlAgilityPack;
using IBApi;
using Skender.Stock.Indicators;




namespace FuturesSystem
{
    public static class FuturesSystem
    {

        private static bool isConnected = false;
        public const int TICK_ID_BASE = 10000000;
        public static EReaderMonitorSignal signal = new EReaderMonitorSignal();
        public static int currentTicker = TICK_ID_BASE;

        public static IBWrapper wrapper;
        public static BigBrain brain = new BigBrain();



        //CHANGE THESE FOR FORWARD CONTRACT ROLLOVER
        public static int contractID = Convert.ToInt32(FuturesEnums.CONTRACT_SYMBOL.ESU2);
        public static FuturesEnums.CONTRACT_SYMBOL theMonthContract = FuturesEnums.CONTRACT_SYMBOL.ESU2;
        //END 


        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("here we gooooo");




                wrapper = new IBWrapper(signal);
                wrapper.bigBrain = brain;

                wrapper.enableAlerts = true;

                wrapper.vix = getVix();

                Console.WriteLine("VIX IS: " + wrapper.vix);

                /*
                if (wrapper.vix > 30)
                {
                  wrapper.sendAlertAsync("VIX too high to trade: " + wrapper.vix);
                }*/


                connect();



                wrapper.ClientSocket.reqAccountSummary(9001, "All", AccountSummaryTags.GetAllTags());


                String queryTime = DateTime.Now.ToString("yyyyMMdd HH:mm:ss"); //pass into reqHistoricalData instead of "" to go back in time, switch live updates to false

                Contract contract = new Contract();
                contract.Symbol = "ES";
                contract.SecType = "FUT";
                contract.Exchange = "GLOBEX";
                contract.Currency = "USD";
                contract.LastTradeDateOrContractMonth = "202209";
                wrapper.contracts.TryAdd(contractID, contract);

               

                Symbol symbol = new Symbol();
                symbol.contract = contract;
                symbol.contractSymbol = theMonthContract;
                symbol.barType = FuturesEnums.BAR_TYPE.FIVE_MINUTE;
                symbol.symbol = FuturesEnums.SYMBOL.ES;

                wrapper.activeSymbols.TryAdd(4002, symbol);

                wrapper.ClientSocket.reqHistoricalData(4002, contract, "", "1 D", "5 mins", "TRADES", 0, 1, false, null);

                wrapper.activeSymbols.TryAdd(19001, symbol);

                wrapper.ClientSocket.reqTickByTickData(19001, contract, "AllLast", 0, false);

                wrapper.ClientSocket.reqOpenOrders();

                wrapper.ClientSocket.reqPositions();

                //wrapper.ClientSocket.reqMktDepthExchanges();

                //watchFuture("ESM2", "GLOBEX", "USD","20220617", "ES");

                // wrapper.ClientSocket.cancelAccountSummary(9002);

                //cancelRequestsInitial(wrapper.ClientSocket);


                //TimeSpan start = TimeSpan.Zero;
                //TimeSpan minutes = TimeSpan.FromMilliseconds(100);

                System.Timers.Timer aTimer = new System.Timers.Timer();
                aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                aTimer.Interval = 100;
                aTimer.Enabled = true;


                //var timer = new System.Threading.Timer(c =>
                //{
                //    wrapper.fiveMinBarTime(19001);
                //}, null, start, minutes);




                while (Console.ReadKey(true).Key != ConsoleKey.Q)
                {
                }

                shutdown();
            }
            catch (Exception ex)
            {
                wrapper.sendAlertAsync("BIG CATCH: " + ex.ToString());
                Console.WriteLine(ex.ToString());
            }
        }

         private static void OnTimedEvent(object source, ElapsedEventArgs e)
         {
            wrapper.fiveMinBarTime(19001, contractID);
        }

        public static decimal getVix()
        {
            decimal vix = 0;

            using (WebClient web1 = new WebClient())
            {
                string data = web1.DownloadString("https://finance.yahoo.com/quote/%5EVIX/");

                var htmlDoc = new HtmlDocument();

                htmlDoc.LoadHtml(data);

                var tr = htmlDoc.DocumentNode.SelectSingleNode("//td[@data-test='PREV_CLOSE-value']");


                string vixVal = tr.InnerText;

                vix = Convert.ToDecimal(vixVal);

                DateTime theDate = DateTime.Now.AddDays(-1);

                using (LocalDatabase db = new LocalDatabase())
                {
                    var sql = "delete from  liveVix where dataDate = '" + theDate.Date.ToShortDateString() + "'";
                    db.insertOrUpdate(sql);

                    sql = "insert into liveVix(dataDate,vix) values('" + theDate.Date.ToShortDateString() + "', " + vix + ")";
                    db.insertOrUpdate(sql);

                }

                return vix;
            }
        }


        private static void connect()
        {
            wrapper.ClientSocket.eConnect("127.0.0.1", 7496, 1);
            var reader = new EReader(wrapper.ClientSocket, signal);
            reader.Start();

            new Thread(() => { while (wrapper.ClientSocket.IsConnected()) { signal.waitForSignal(); reader.processMsgs(); } }) { IsBackground = true }.Start();
        }

        private static void cancelRequestsInitial(EClientSocket ecs)
        {
            //cancel all reqs
            for (int i = TICK_ID_BASE; i < 100; i++)
            {
                ecs.cancelMktData(i);

            }
        }

        private static void shutdown()
        {
            wrapper.ClientSocket.cancelAccountSummary(9001);

            //cancel all reqs
            for (int i = TICK_ID_BASE; i <= currentTicker; i++)
            {
                wrapper.ClientSocket.cancelMktData(currentTicker);

            }

            Console.WriteLine("------------ ALL CLOSED ------------");
        }


        private static void watchInstrument(string symbol, string exchange, string currency)
        {
            Contract c = addContractBySymbol(symbol, exchange, currency);

        }

        private static void watchFuture(string symbol, string exchange, string currency, string lastTradeDay, string tradingClass)
        {
            Contract c = addFutureBySymbol(symbol, exchange, currency, lastTradeDay, tradingClass);

        }


        private static Contract addContractBySymbol(string symbol, string exchange, string currency)
        {
            Contract contract = new Contract();
            contract.Symbol = symbol;
            contract.Exchange = exchange;
            contract.SecType = "STK";
            contract.Currency = currency;



            //contract.tickID = currentTicker;


            wrapper.ClientSocket.reqMktData(currentTicker, contract, "", false, false, new List<TagValue>());


            //wrapper.activeSymbols.Add(currentTicker, contract);


            currentTicker += 1;

            return contract;
        }

        private static Contract addFutureBySymbol(string symbol, string exchange, string currency, string lastTradeDay, string tradingClass)
        {
            Contract contract = new Contract();
            contract.Symbol = symbol;
            contract.Exchange = exchange;
            contract.SecType = "FUT";
            contract.Currency = currency;
            contract.LastTradeDateOrContractMonth = lastTradeDay;
            contract.TradingClass = tradingClass;
            //contract.tickID = currentTicker;


            wrapper.ClientSocket.reqMktData(currentTicker, contract, "", false, false, new List<TagValue>());

            //wrapper.activeRequests.Add(currentTicker, contract);


            currentTicker += 1;

            return contract;
        }



        private static Contract addFx(string symbol, string currency)
        {
            Contract contract = new Contract();
            contract.Symbol = symbol;
            contract.Exchange = "IDEALPRO";
            contract.SecType = "CASH";
            contract.Currency = currency;
            //contract.tickID = currentTicker;


            wrapper.ClientSocket.reqMktData(currentTicker, contract, "", false, false, new List<TagValue>());

            //wrapper.activeRequests.Add(currentTicker, contract);

            currentTicker += 1;

            return contract;
        }




    }

}
