using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuturesSystem
{
  public class FuturesEnums
  {
    public enum BAR_TYPE { ONE_MINUTE = 4, FIVE_MINUTE = 3 }
    public enum SYMBOL { ES = 1 }
    public enum CONTRACT_SYMBOL { ESM2 = 1, ESU2=2 }

    public enum OPEN_OR_CLOSE  {OPEN = 1, CLOSE=2}


  }
}
