using IBApi;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuturesSystem
{
    public class LocalDatabase : IDisposable
    {
        private string mySqlConnectionString = "";

        public SqlConnection myConnectionLocal;
        public SqlCommand myCommandLocal;
        public SqlDataReader myReaderLocal = null;
        public SqlTransaction myTransLocal = null;

        public LocalDatabase()
        {
            //initialize("Data Source=192.168.1.29;Initial Catalog=Backtest;User ID=toby;Password=toby; ");
            initialize("Data Source=localhost;Initial Catalog=Backtest;User ID=toby;Password=toby; ");
        }

        public LocalDatabase(string connectionString)
        {
            initialize(connectionString);
        }


        public void insertHistoricalData(Bar bar, Symbol s)
        {


            int symbolID = Convert.ToInt32(s.symbol);
            int barTypeID = Convert.ToInt32(s.barType);
            int contractSymbolID = Convert.ToInt32(s.contractSymbol);


            string sql = $"delete from combinedFuturesData where symbolID = {symbolID} and barType = {barTypeID} and dataDate = '{bar.Time}'";
            insertOrUpdate(sql);

            sql = $"insert into combinedFuturesData ([open], high, low, [close], vol, dataDate, symbolID, barType, contractSymbolID) values ({bar.Open}, {bar.High}, {bar.Low}, {bar.Close}, {bar.Volume}, '{bar.Time}',{symbolID},{barTypeID}, {contractSymbolID}) ";
            insertOrUpdate(sql);

        }



        public void updateOrderID(int orderID)
        {

            string sql = $"update ibSettings set orderID = {orderID} ";
            insertOrUpdate(sql);

        }



        public long addFutureToDB(Future f)
        {
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(f.indicatorJSON);


            var sql = "";


            sql = @"insert into ironcondors (runID, commission, dateCreated, strikeA,strikeB, expiryA,expiryB, priceA, priceB,  callPutA, callPutB, underlyingDate,underlyingPrice, indicatorJSON, tradeType, volA, volB, contracts )";
            sql += $" VALUES ({-1}, {f.commission}, '{ f.inBar.Date}', { f.inBar.Open}, null,  ";
            sql += $"'{f.inBar.Date}',null,  {f.inBar.Open}, null,   ";
            sql += $"{ 1 }, { 1}, '{f.inBar.Date}', {f.inBar.Open}, '{json}', {Convert.ToInt32(f.tradeType)}, {f.inBar.Volume}, null, {f.contracts} )";


            myCommandLocal.CommandText = sql;
            return insertWithIdentity(sql);

        }



        public void updateFuture(Future f, decimal avgFillPrice, decimal Volume, DateTime Date, decimal bestPossiblePrice)
        {
            using (LocalDatabase ldb = new LocalDatabase())
            {
                var sql = "";

                sql = $"update ironcondors set strikeA = {avgFillPrice}, expiryA = '{Date}' , priceA = {avgFillPrice}, volA = {Volume}, avgFillPrice = {avgFillPrice}, bestPossiblePrice={bestPossiblePrice} where id = " + f.id;

                myCommandLocal.CommandText = sql;
                insertOrUpdate(sql);

            }

        }

        public void updateFutureSell(Future f, decimal avgFillPrice, decimal Volume, DateTime Date, decimal bestPossiblePrice)
        {
            using (LocalDatabase ldb = new LocalDatabase())
            {
                var sql = "";

                sql = $"update ironcondors set strikeB = {avgFillPrice}, expiryB = '{Date}' , priceB = {avgFillPrice}, volB = {Volume}, avgFillPriceEnd = {avgFillPrice}, bestPossiblePriceEnd={bestPossiblePrice} where id = " + f.id;

                myCommandLocal.CommandText = sql;
                insertOrUpdate(sql);

            }

        }

        public void addSettlementToDB(Settlement s)
        {

            var sql = $"insert into settlements (settlementType, pl, dateCreated, runID, ironcondorID, underlyingPrice,underlyingDate, contracts, commission, positionOpen) values('{Convert.ToInt32(s.settlementType)}', {s.profitLoss}, '{s.dateCreated}', -1, {s.ironCondorID}, {s.underlyingPrice}, '{s.underlyingDate}', {s.contracts}, {s.commission}, '{s.inTime}'); ";
            myCommandLocal.CommandText = sql;
            insertOrUpdate(sql);


        }

        public int getNextOrderId()
        {
            int orderID = 0;

            string sql = "select orderID from ibSettings";

            try
            {
                myCommandLocal.CommandText = sql;
                myReaderLocal = myCommandLocal.ExecuteReader();

                if (myReaderLocal.Read())
                {


                    orderID = Convert.ToInt32(myReaderLocal["orderID"].ToString());


                }
            }
            catch (Exception e)
            {
                throw;
            }

            return orderID;

        }



        private void initialize(string connectionString)
        {
            try
            {
                myConnectionLocal = new SqlConnection(connectionString);
                myConnectionLocal.Open();
                myCommandLocal = myConnectionLocal.CreateCommand();
                myCommandLocal.Connection = myConnectionLocal;
                myReaderLocal = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("BIG ERROR DB:" + e.Message);
            }
        }

        public long insertWithIdentity(string sql)
        {

            long retVal = 0;

            sql += " SELECT SCOPE_IDENTITY()";

            try
            {
                string sqlstring = sql;
                myCommandLocal.CommandText = sqlstring;
                retVal = Convert.ToInt64(myCommandLocal.ExecuteScalar());
            }
            catch (Exception e)
            {

            }

            return retVal;
        }


        public void insertOrUpdate(string sql)
        {
            try
            {
                string sqlstring = sql;
                myCommandLocal.CommandText = sqlstring;
                myCommandLocal.ExecuteNonQuery();
            }
            catch (Exception e)
            {

            }

        }


        public void insertOrUpdateAsync(string sql)
        {
            try
            {
                myCommandLocal.CommandText = sql;
                myCommandLocal.ExecuteNonQuery();
            }
            catch (Exception e)
            {

            }

        }

        public DataTable SelectDataTable(SqlCommand command)
        {
            DataTable dataTable = null;
            try
            {
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = command;
                dataTable = new DataTable();
                sqlDataAdapter.Fill(dataTable);
            }
            catch (Exception e)
            {
                string debug = command.CommandText;
                Console.WriteLine(e.Message);
                // throw e;//
            }

            return dataTable;
        }

        public void Dispose()
        {
            try
            {
                myConnectionLocal.Dispose();
            }
            catch (Exception e)
            { }
        }
    }
}
