using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using IBApi;
using IBSampleApp.messages;
using Newtonsoft.Json;
using Skender.Stock.Indicators;

namespace FuturesSystem
{
    public class IBWrapper : EWrapper
    {
        private EClientSocket clientSocket;
        private int nextOrderId;
        private int clientId;
        public Dictionary<int, Symbol> activeSymbols = new Dictionary<int, Symbol>();
        public BigBrain bigBrain;
        public bool sentRestartAlert = false;

        public static DateTime epocDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

        //public ConcurrentDictionary<string, IndexQuote> rollingData = new ConcurrentDictionary<string, IndexQuote>();
        public Dictionary<string, IndexQuote> rollingData = new Dictionary<string, IndexQuote>();
        public ConcurrentDictionary<string, IEnumerable<HeikinAshiResult>> haCandles = new ConcurrentDictionary<string, IEnumerable<HeikinAshiResult>>();
        public ConcurrentDictionary<string, IEnumerable<AtrResult>> atr = new ConcurrentDictionary<string, IEnumerable<AtrResult>>();

        public bool enableAlerts = false;

        public List<Future> activeFutures = new List<Future>();

        public ConcurrentDictionary<int, Trade> trades = new ConcurrentDictionary<int, Trade>();

        public ConcurrentDictionary<int, Contract> contracts = new ConcurrentDictionary<int, Contract>();

        public int lastMins = -1;
        public bool firstBar = true;
        public DateTime lastTradeDate = DateTime.MinValue;
        public DateTime lastExit = DateTime.MinValue;

        public bool inTrade = false;

        public decimal vix = 0;



        public int ClientId
        {
            get { return clientId; }
            set { clientId = value; }
        }

        SynchronizationContext sc;

        public IBWrapper(EReaderSignal signal)
        {
            clientSocket = new EClientSocket(this, signal);
            sc = SynchronizationContext.Current;

            using (LocalDatabase db = new LocalDatabase())
            {
                nextOrderId = db.getNextOrderId(); //set initial orderID from db
            }

        }

        public void fiveMinBarTime(int symbolReqID, int contractID)
        {

            DateTime now = DateTime.Now;

            DateTime theDate = now.Date;

            int minuteMain = now.Minute / 10;
            int minuteRem = now.Minute % 10;

            TimeSpan ts;

            if (minuteRem < 5)
            {
                ts = new TimeSpan(now.Hour, minuteMain * 10, 0);
            }
            else
                ts = new TimeSpan(now.Hour, minuteMain * 10 + 5, 0);

            theDate = theDate + ts;

            Symbol s = activeSymbols[symbolReqID];

            string key = getIndexKey(s.symbol, theDate.ToString("yyyyMMdd  HH:mm:ss"));

            var currMins = theDate.Minute;

            bool newFiveMinBar = false;

            IndexQuote lastBar = new IndexQuote();
            

            if (currMins != lastMins)
            {
               

                if (currMins % 5 == 0)
                {

                    string lastBarKey = getIndexKey(s.symbol, theDate.AddMinutes(-5).ToString("yyyyMMdd  HH:mm:ss"));

                    if (rollingData.ContainsKey(lastBarKey))
                    {

                        lastBar = rollingData[lastBarKey];

                        //todo: if running overnight maybe change this but it sometimes doesnt get updates for more than a few mins after hours, maybe adjust the 30 secs to x mins after hours
                        if ((DateTime.Now - lastBar.lastUpdated).TotalSeconds > 30)
                        {
                            if (lastMins != -1)
                            {
                                //big panic!
                                string cancelTrades = "";
                                Console.WriteLine("Data older than 30 seconds!!");
                                if(DateTime.Now.Hour > 9 && DateTime.Now.Hour < 18)
                                    sendAlertAsync("Data older than 30 seconds between bar switch! Check open trade! lastKey: " + lastBarKey + ", Current Time:" + DateTime.Now.ToString());
                            }

                        }
                        else
                        {
                            newFiveMinBar = true;
                        }
                    }
                } 
                lastMins = currMins;
            }


            if (newFiveMinBar)
            {
                Console.WriteLine("New 5 min bar:  " + theDate + " ... " + DateTime.Now.ToString("hh.mm.ss.ffffff"));
                Console.WriteLine("Last bar:  " + lastBar.Date + " ... " + lastBar.Open + "|" + lastBar.High + "|" + lastBar.Low + "|" + lastBar.Close);


                IndexQuote currQuote = new IndexQuote();

                if (rollingData.ContainsKey(key))
                {
                    currQuote = rollingData[key];
                }
                else //now we run our own 5min bar checker, so if they haven't pushed one yet use last info, we only need to trigger the date anyways
                {
                    currQuote = new IndexQuote();
                    //DateTime newDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, 0);
                    currQuote.Date = theDate; //newDate;
                    rollingData[key] = currQuote;
                }

                //if brand new bar, run strategy
                if (activeFutures.Count == 0 && newFiveMinBar)
                {

                    // figure out how many bars it needs to recalculate and just recalculate those for speed

                    Future future = Strategies.runTwoHAEntry(currQuote, lastExit, rollingData, vix);

                    if (future.strategyPassed)
                    {
                        try
                        {
                            //try to make trade
                            getNextOrderID();
                            Contract c = contracts[contractID];

                            future.contracts = 1;
                            activeFutures.Add(future);
                            Trade trade = new Trade();
                            trade.orderID = nextOrderId;
                            trade.requestedVolume = future.contracts;
                            trades.TryAdd(nextOrderId, trade);
                            trade.openOrClose = FuturesEnums.OPEN_OR_CLOSE.OPEN;
                            trade.future = future;

                            //future.tradeType = Future.TradeType.BUY; //TESTING REMOVE!@#!@#!@#!@#

                            lastTradeDate = currQuote.Date;

                            future.openTrade = trade;
                            future.inBar = currQuote;

                            Order order = GetMarketOrder(future.tradeType.ToString(), trade.requestedVolume);

                            Console.WriteLine("SENDING ENTRY: " + DateTime.Now.ToString("hh.mm.ss.ffffff"));
                            clientSocket.placeOrder(nextOrderId, c, order);


                            if(future.id == 0)
                            { 
                                using (LocalDatabase ldb = new LocalDatabase())
                                {
                                    future.id = ldb.addFutureToDB(future);
                                }
                            }
                           
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("ERROR SENDING ORDER: " +  ex.ToString());
                        }


                        //save currbar open vs market fill price to see

                        //wrapper.sendAlertAsync("test test");
                    }
                }
                else if (activeFutures.Count > 0)
                {
                    //IndexQuote currQuote = rollingData[key];

                    if(currQuote.Close == 0)
                    {
                        string lastBarKey = getIndexKey(s.symbol, theDate.AddMinutes(-5).ToString("yyyyMMdd  HH:mm:ss"));
                        currQuote.Close = rollingData[lastBarKey].Close;
                    }

                    List<IndexQuote> indexQuotes = rollingData.Values.ToList();
                    List<HeikinAshiResult> haCandles = indexQuotes.GetHeikinAshi().ToList(); //Take(50).

                    //check if time to sell
                    if (currQuote.Date != lastTradeDate && !inTrade && Strategies.exitTwoHA(currQuote, haCandles, indexQuotes, activeFutures, this))
                    {
                        Future future = activeFutures.FirstOrDefault();

                        Console.WriteLine("SENDING EXIT1: " + DateTime.Now.ToString("hh.mm.ss.ffffff") + " filled: " + future.openTrade.filledVolume + " requested: " + future.openTrade.requestedVolume);

                        if (future.openTrade.filledVolume > 0 && future.openTrade.filledVolume == future.openTrade.requestedVolume)
                        {
                            string closeType = "SELL";

                            if (future.tradeType == Future.TradeType.SELL)
                                closeType = "BUY";

                            Order order = GetMarketOrder(closeType, future.openTrade.filledVolume);

                            Contract c = contracts[contractID];

                            Trade trade = new Trade();
                            trade.orderID = getNextOrderID();
                            trade.requestedVolume = future.openTrade.filledVolume;
                            trade.openOrClose = FuturesEnums.OPEN_OR_CLOSE.CLOSE;
                            trade.future = future;
                            trades.TryAdd(nextOrderId, trade);
                            future.outBar = currQuote;
                            inTrade = true;
                            lastExit = DateTime.Now;
                            clientSocket.placeOrder(nextOrderId, c, order);

                            Console.WriteLine("SENDING EXIT2: " + DateTime.Now.ToString("hh.mm.ss.ffffff"));
                        }

                    }
                    else
                    {
                        //maybe send alert?
                    }


                }


            }


        }

        public int getNextOrderID()
        {
            using (LocalDatabase db = new LocalDatabase())
            {
                nextOrderId = nextOrderId + 1;
                db.updateOrderID(nextOrderId);

                return nextOrderId;
            }

        }

        public EClientSocket ClientSocket
        {
            get { return clientSocket; }
            private set { clientSocket = value; }
        }

        public int NextOrderId
        {
            get { return nextOrderId; }
            set { nextOrderId = value; }
        }

        //start override

        public virtual void error(Exception e)
        {
            Console.WriteLine("ERRORRRRR A!!! " + e.ToString());
        }

        public virtual void error(string str)
        {
            Console.WriteLine("ERRORRRRR B!!! " + str);
        }

        public virtual void error(int id, int errorCode, string errorMsg)
        {
            Console.WriteLine("DEBUG INFO [ " + id + " | " + errorCode + " | " + errorMsg + "]");
        }

        public virtual void currentTime(long time)
        {
            Console.WriteLine("time [" + time + "]");
        }

        public virtual void tickPrice(int tickerId, int field, double price, TickAttrib attribs)
        {
            Console.WriteLine("tickPrice [" + tickerId + " | " + tickerId + " | " + TickType.getField(field) + " | " + price + " | " + attribs.ToString() + "]");



            // Console.BackgroundColor = ConsoleColor.Gray;
            //Console.ForegroundColor = ConsoleColor.DarkMagenta;

            //Console.WriteLine("NEW CALC -- " + pinxInstrument.pinx.contract.Symbol + "," + pinxInstrument.main.contract.Symbol + " | Offer: " + pinxInstrument.offerPercent.ToString("#0.000") + "%" + " Best Actual: " + pinxInstrument.bestPercent.ToString("#0.000") + "%" + " Direction: " + pinxInstrument.direction.ToString());

            //Console.BackgroundColor = ConsoleColor.Black;
            //Console.ForegroundColor = ConsoleColor.White;


        }

        public virtual void tickSize(int tickerId, int field, int size)
        {
            Console.WriteLine("tickSize [" + tickerId + " | " + tickerId + " | " + TickType.getField(field) + " | " + size + "]");
        }

        public virtual void tickString(int tickerId, int field, string value)
        {
            Console.WriteLine("tickString [" + tickerId + " | " + tickerId + " | " + TickType.getField(field) + " | " + value + "]");
        }

        public virtual void tickGeneric(int tickerId, int field, double value)
        {
            Console.WriteLine("tickGeneric [" + tickerId + " | " + tickerId + " | " + TickType.getField(field) + " | " + value + "]");
        }

        public virtual void tickEFP(int tickerId, int tickType, double basisPoints, string formattedBasisPoints, double impliedFuture, int holdDays, string futureLastTradeDate, double dividendImpact, double dividendsToLastTradeDate)
        {

        }

        public virtual void deltaNeutralValidation(int reqId, DeltaNeutralContract deltaNeutralContract)
        {

        }

        public virtual void tickOptionComputation(int tickerId, int field, double impliedVolatility, double delta, double optPrice, double pvDividend, double gamma, double vega, double theta, double undPrice)
        {

        }

        public virtual void tickSnapshotEnd(int tickerId)
        {

        }

        public virtual void nextValidId(int orderId)
        {

        }

        public virtual void managedAccounts(string accountsList)
        {

        }

        public virtual void connectionClosed()
        {
            Console.WriteLine("CONNECTION LOST!@#");
        }



        public virtual void bondContractDetails(int reqId, ContractDetails contract)
        {

        }

        public virtual void updateAccountValue(string key, string value, string currency, string accountName)
        {

        }

        public virtual void updatePortfolio(Contract contract, double position, double marketPrice, double marketValue, double averageCost, double unrealizedPNL, double realizedPNL, string accountName)
        {

        }

        public virtual void updateAccountTime(string timestamp)
        {

        }

        public virtual void accountDownloadEnd(string account)
        {

        }

        public virtual void orderStatus(int orderId, string status, decimal filled, decimal remaining, double avgFillPrice, int permId, int parentId, double lastFillPrice, int clientId, string whyHeld, double mktCapPrice)
        {
            Console.WriteLine("OrderStatus." + DateTime.Now.ToString("hh.mm.ss.ffffff") + "  Id: " + orderId + ", Status: " + status + ", Filled: " + Util.DecimalMaxString(filled) + ", Remaining: " + Util.DecimalMaxString(remaining)
                + ", AvgFillPrice: " + Util.DoubleMaxString(avgFillPrice) + ", PermId: " + Util.IntMaxString(permId) + ", ParentId: " + Util.IntMaxString(parentId) +
                ", LastFillPrice: " + Util.DoubleMaxString(lastFillPrice) + ", ClientId: " + Util.IntMaxString(clientId) + ", WhyHeld: " + whyHeld + ", MktCapPrice: " + Util.DoubleMaxString(mktCapPrice));

            if (trades.ContainsKey(orderId))
            {

                Trade t = trades[orderId];
                // f.orderState = orderState;
                t.filledVolume = filled;
                t.avgFillPrice = Convert.ToDecimal(avgFillPrice);
                t.orderStatus = status;
                Future future = activeFutures.FirstOrDefault();

                if (t.requestedVolume == t.filledVolume && t.orderStatus == "Filled")
                {
                    using (LocalDatabase db = new LocalDatabase())
                    {

                        if (t.openOrClose == FuturesEnums.OPEN_OR_CLOSE.CLOSE)
                        {
                            decimal open = future.outBar.Open;
                            db.updateFutureSell(future, Convert.ToDecimal(avgFillPrice), filled, DateTime.Now, open);
                        }
                        else
                        {
                            if (future.id == 0)
                            {
                                using (LocalDatabase ldb = new LocalDatabase())
                                {
                                    future.id = ldb.addFutureToDB(future);
                                }
                            }

                            decimal open = future.inBar.Open;
                            db.updateFuture(future, Convert.ToDecimal(avgFillPrice), filled, DateTime.Now, open);
                        }
                    }
                }


                if (t.orderStatus == "Filled" && t.requestedVolume == t.filledVolume && t.openOrClose == FuturesEnums.OPEN_OR_CLOSE.CLOSE && inTrade)
                {
                    Settlement s = new Settlement();
                    s.dateCreated = DateTime.Now;
                    s.commission = Convert.ToDecimal(0.5) * 2;
                    s.future = future;

                    if (s.future.longOrShort == Future.LONG_OR_SHORT.LONG)
                        s.profitLoss = (t.avgFillPrice - future.openTrade.avgFillPrice) * 50;
                    else
                        s.profitLoss = (t.avgFillPrice - future.openTrade.avgFillPrice) * -1 * 50;

                    s.inTime = s.future.inBar.Date;
                    s.outTime = s.future.outBar.Date;
                    s.settlementType = Settlement.SettlementType.Exited;
                    s.ironCondorID = s.future.id;
                    s.underlyingPrice = Convert.ToDecimal(avgFillPrice);
                    s.underlyingDate = DateTime.Now;
                    s.contracts = s.future.contracts;

                    using (LocalDatabase db = new LocalDatabase())
                    {
                        db.addSettlementToDB(s);
                    }

                    sendAlertAsync("Trade Completed: $" + s.profitLoss);

                    inTrade = false;

                    activeFutures.Clear();
                    trades.Clear();

                }
            }

        }

        public virtual void openOrder(int orderId, Contract contract, Order order, OrderState orderState)
        {
            Console.WriteLine("OpenOrder. PermID: " + Util.IntMaxString(order.PermId) + ", ClientId: " + Util.IntMaxString(order.ClientId) + ", OrderId: " + Util.IntMaxString(orderId) +
                ", Account: " + order.Account + ", Symbol: " + contract.Symbol + ", SecType: " + contract.SecType + " , Exchange: " + contract.Exchange + ", Action: " + order.Action +
                ", OrderType: " + order.OrderType + ", TotalQty: " + Util.DecimalMaxString(order.TotalQuantity) + ", CashQty: " + Util.DoubleMaxString(order.CashQty) +
                ", LmtPrice: " + Util.DoubleMaxString(order.LmtPrice) + ", AuxPrice: " + Util.DoubleMaxString(order.AuxPrice) + ", Status: " + orderState.Status +
                ", MinTradeQty: " + Util.IntMaxString(order.MinTradeQty) + ", MinCompeteSize: " + Util.IntMaxString(order.MinCompeteSize) +
                ", CompeteAgainstBestOffset: " + (order.CompeteAgainstBestOffset == Order.COMPETE_AGAINST_BEST_OFFSET_UP_TO_MID ? "UpToMid" : Util.DoubleMaxString(order.CompeteAgainstBestOffset)) +
                ", MidOffsetAtWhole: " + Util.DoubleMaxString(order.MidOffsetAtWhole) + ", MidOffsetAtHalf: " + Util.DoubleMaxString(order.MidOffsetAtHalf));
        }

        public virtual void openOrderEnd()
        {

        }

        public virtual void contractDetails(int reqId, ContractDetails contractDetails)
        {

        }

        public virtual void contractDetailsEnd(int reqId)
        {

        }

        public virtual void execDetails(int reqId, Contract contract, Execution execution)
        {
            Console.WriteLine("execDetails: " + execution.OrderId + ", $" + execution.AvgPrice + " #" + execution.CumQty);

            //record the open fill price incase order status missed it
            if (trades.ContainsKey(execution.OrderId))
            {
                Trade t = trades[execution.OrderId];

                t.filledVolume = execution.CumQty;
                t.avgFillPrice = Convert.ToDecimal(execution.AvgPrice);
                Future future = activeFutures.FirstOrDefault();

                if (t.requestedVolume == t.filledVolume)
                {
                    using (LocalDatabase db = new LocalDatabase())
                    {
                        if (t.openOrClose != FuturesEnums.OPEN_OR_CLOSE.CLOSE)
                        { 
                            decimal open = future.inBar.Open;

                            db.updateFuture(future, Convert.ToDecimal(t.avgFillPrice), t.filledVolume, DateTime.Now, open);
                        }
                    }
                }
            }
        }

        public virtual void execDetailsEnd(int reqId)
        {

        }

        public virtual void commissionReport(CommissionReport commissionReport)
        {

        }

        public virtual void fundamentalData(int reqId, string data)
        {

        }

        public virtual void historicalData(int reqId, Bar bar)
        {
            // Console.WriteLine("HistoricalData. " + reqId + " - Time: " + bar.Time + ", Open: " + Util.DoubleMaxString(bar.Open) + ", High: " + Util.DoubleMaxString(bar.High) +
            //    ", Low: " + Util.DoubleMaxString(bar.Low) + ", Close: " + Util.DoubleMaxString(bar.Close) + ", Volume: " + Util.DecimalMaxString(bar.Volume) +
            //    ", Count: " + Util.IntMaxString(bar.Count) + ", WAP: " + Util.DecimalMaxString(bar.WAP));

            Symbol s = activeSymbols[reqId];


            string key = getIndexKey(s.symbol, bar.Time);

            IndexQuote q = barToIndexQuote(bar);

            using (LocalDatabase db = new LocalDatabase())
            {
                db.insertHistoricalData(bar, s);
            }


            if (rollingData.ContainsKey(key))
            {
                rollingData[key] = q;
            }
            else
            {
                //rollingData.TryAdd(key, q);
                rollingData.Add(key, q);
            }


        }


        public void sendAlertAsync(string text)
        {

            if (enableAlerts)
            {
                string url = "https://hooks.slack.com/services/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";

                Uri uri = new Uri(url);

                Payload payload = new Payload()
                {
                    Channel = "trade-alerts",
                    Username = "dave",
                    Text = text
                };

                string payloadJson = JsonConvert.SerializeObject(payload);

                using (WebClient client = new WebClient())
                {
                    NameValueCollection data = new NameValueCollection();
                    data["payload"] = payloadJson;
                    client.UploadValuesAsync(uri, "POST", data);

                }
            }

        }


        public void historicalDataUpdate(int reqId, Bar bar)
        {
            // Console.WriteLine("HistoricalDataUpdate. " + reqId + " - Time: " + bar.Time + ", Open: " + Util.DoubleMaxString(bar.Open) + ", High: " + Util.DoubleMaxString(bar.High) +
            //    ", Low: " + Util.DoubleMaxString(bar.Low) + ", Close: " + Util.DoubleMaxString(bar.Close) + ", Volume: " + Util.DecimalMaxString(bar.Volume) +
            //    ", Count: " + Util.IntMaxString(bar.Count) + ", WAP: " + Util.DecimalMaxString(bar.WAP));

            // Console.WriteLine("Data Update. " + DateTime.Now.ToString("hh.mm.ss.ffffff") + " - " + bar.Time + ", O: " + Util.DoubleMaxString(bar.Open) + ", H: " + Util.DoubleMaxString(bar.High) +
            // ", L: " + Util.DoubleMaxString(bar.Low) + ", C: " + Util.DoubleMaxString(bar.Close) + ", V: " + Util.DecimalMaxString(bar.Volume));
            //    ", Count: " + Util.IntMaxString(bar.Count) + ", WAP: " + Util.DecimalMaxString(bar.WAP));

            Symbol s = activeSymbols[reqId];

            string key = getIndexKey(s.symbol, bar.Time);


            IndexQuote currQuote = barToIndexQuote(bar);
            currQuote.lastUpdated = DateTime.Now;

            if (rollingData.ContainsKey(key))
                rollingData[key] = currQuote;
            else
                rollingData.Add(key, currQuote); //rollingData.TryAdd(key, currQuote);

            var currMins = currQuote.Date.Minute;

            bool newFiveMinBar = false;

            if (currMins != lastMins)
            {

                if (currMins % 5 == 0)
                {
                    string lastBarKey = getIndexKey(s.symbol, DateTime.ParseExact(bar.Time, "yyyyMMdd  HH:mm:ss", null).AddMinutes(-5).ToString("yyyyMMdd  HH:mm:ss"));

                    if ((DateTime.Now - rollingData[lastBarKey].lastUpdated).TotalSeconds > 30)
                    {

                        if (lastMins != -1)
                        {
                            //big panic!
                            string cancelTrades = "";

                            sendAlertAsync("Data older than 30 seconds between bar switch! (Cancel open trades?) lastKey: " + lastBarKey + ", Current Time:" + DateTime.Now.ToString());
                        }

                    }
                    else
                    {
                        newFiveMinBar = true;
                    }
                }
                lastMins = currMins;

            }


            if (newFiveMinBar)
                Console.WriteLine("New 5 min bar:  " + currQuote.Date + " ... " + DateTime.Now.ToString("hh.mm.ss.ffffff"));

            //if brand new bar, run strategy
            if (activeFutures.Count == 0 && newFiveMinBar)
            {

                // figure out how many bars it needs to recalculate and just recalculate those for speed

                Future future = Strategies.runTwoHAEntry(currQuote, lastExit, rollingData, vix);

                if (future.strategyPassed)
                {

                    //try to make trade
                    getNextOrderID();
                    Contract c = contracts[Convert.ToInt32(FuturesEnums.CONTRACT_SYMBOL.ESM2)];

                    future.contracts = 1;
                    activeFutures.Add(future);
                    Trade trade = new Trade();
                    trade.orderID = nextOrderId;
                    trade.requestedVolume = future.contracts;
                    trades.TryAdd(nextOrderId, trade);
                    trade.openOrClose = FuturesEnums.OPEN_OR_CLOSE.OPEN;
                    trade.future = future;

                    //future.tradeType = Future.TradeType.BUY; //TESTING REMOVE!@#!@#!@#!@#

                    lastTradeDate = currQuote.Date;

                    future.openTrade = trade;
                    future.inBar = currQuote;

                    Order order = GetMarketOrder(future.tradeType.ToString(), trade.requestedVolume);

                    clientSocket.placeOrder(nextOrderId, c, order);

                    using (LocalDatabase ldb = new LocalDatabase())
                    {
                        future.id = ldb.addFutureToDB(future);
                    }

                    Console.WriteLine("SENDING ENTRY: " + DateTime.Now.ToString("hh.mm.ss.ffffff"));


                    //save currbar open vs market fill price to see

                    //wrapper.sendAlertAsync("test test");
                }
            }
            else if (activeFutures.Count > 0)
            {
                List<IndexQuote> indexQuotes = rollingData.Values.ToList();
                List<HeikinAshiResult> haCandles = indexQuotes.GetHeikinAshi().ToList(); //Take(50).

                //check if time to sell
                if (currQuote.Date != lastTradeDate && !inTrade && Strategies.exitTwoHA(currQuote, haCandles, indexQuotes, activeFutures, this))
                {
                    Future future = activeFutures.FirstOrDefault();

                    Console.WriteLine("SENDING EXIT1: " + DateTime.Now.ToString("hh.mm.ss.ffffff") + " filled: " + future.openTrade.filledVolume + " requested: " + future.openTrade.requestedVolume);

                    if (future.openTrade.filledVolume > 0 && future.openTrade.filledVolume == future.openTrade.requestedVolume)
                    {
                        string closeType = "SELL";

                        if (future.tradeType == Future.TradeType.SELL)
                            closeType = "BUY";

                        Order order = GetMarketOrder(closeType, future.openTrade.filledVolume);

                        Contract c = contracts[Convert.ToInt32(FuturesEnums.CONTRACT_SYMBOL.ESM2)];

                        Trade trade = new Trade();
                        trade.orderID = getNextOrderID();
                        trade.requestedVolume = future.openTrade.filledVolume;
                        trade.openOrClose = FuturesEnums.OPEN_OR_CLOSE.CLOSE;
                        trade.future = future;
                        trades.TryAdd(nextOrderId, trade);
                        future.outBar = currQuote;
                        inTrade = true;
                        lastExit = DateTime.Now;
                        clientSocket.placeOrder(nextOrderId, c, order);

                        Console.WriteLine("SENDING EXIT2: " + DateTime.Now.ToString("hh.mm.ss.ffffff"));
                    }

                }
                else
                {
                    //maybe send alert?
                }


            }

        }

        public Order GetOrder(string action, string orderType, int totalQty, double price)
        {
            Order order = new Order();
            order.Action = action;
            order.OrderType = orderType;
            order.TotalQuantity = totalQty;
            order.LmtPrice = price;
            return order;
        }

        public Order GetMarketOrder(string action, decimal totalQty)
        {

            Order order = new Order();
            order.Action = action;
            order.OrderType = "MKT";
            order.TotalQuantity = Convert.ToInt32(totalQty);

            return order;
        }

        public string getIndexKey(FuturesEnums.SYMBOL symbol, string time)
        {

            // int symbolID = Convert.ToInt32(symbol);
            //return symbolID.ToString() + "|" + time;
            return symbol + "|" + time;

        }

        public IndexQuote barToIndexQuote(Bar bar)
        {
            IndexQuote q = new IndexQuote();
            q.Open = Convert.ToDecimal(bar.Open);
            q.High = Convert.ToDecimal(bar.High);
            q.Low = Convert.ToDecimal(bar.Low);
            q.Close = Convert.ToDecimal(bar.Close);
            q.Volume = Convert.ToDecimal(bar.Volume);

            //20220405  16:00:00
            q.Date = DateTime.ParseExact(bar.Time, "yyyyMMdd  HH:mm:ss", null);

            return q;

        }


        public virtual void historicalDataEnd(int reqId, string startDate, string endDate)
        {
            Console.WriteLine("HistoricalDataEnd - " + reqId + " from " + startDate + " to " + endDate);
        }

        public virtual void marketDataType(int reqId, int marketDataType)
        {

        }

        public virtual void updateMktDepth(int tickerId, int position, int operation, int side, double price, int size)
        {
            Console.WriteLine("UpdateMarketDepth. " + tickerId + " - Position: " + position + ", Operation: " + operation + ", Side: " + side + ", Price: " + Util.DoubleMaxString(price) + ", Size: " + Util.DecimalMaxString(size));
        }

        public virtual void updateMktDepthL2(int tickerId, int position, string marketMaker, int operation, int side, double price, int size, bool isSmartDepth)
        {
            Console.WriteLine("UpdateMarketDepthL2. " + tickerId + " - Position: " + position + ", Operation: " + operation + ", Side: " + side + ", Price: " + Util.DoubleMaxString(price) + ", Size: " + Util.DecimalMaxString(size) + ", isSmartDepth: " + isSmartDepth);
        }

        public virtual void updateNewsBulletin(int msgId, int msgType, string message, string origExchange)
        {

        }

        public virtual void position(string account, Contract contract, double pos, double avgCost)
        {

        }

        public virtual void positionEnd()
        {
            Console.WriteLine("PositionEnd.");
        }

        public virtual void realtimeBar(int reqId, long date, double open, double high, double low, double close, long volume, double WAP, int count)
        {

        }

        public virtual void scannerParameters(string xml)
        {

        }

        public virtual void scannerData(int reqId, int rank, ContractDetails contractDetails, string distance, string benchmark, string projection, string legsStr)
        {

        }

        public virtual void scannerDataEnd(int reqId)
        {

        }

        public virtual void receiveFA(int faDataType, string faXmlData)
        {

        }

        public virtual void verifyMessageAPI(string apiData)
        {

        }

        public virtual void verifyCompleted(bool isSuccessful, string errorText)
        {

        }

        public virtual void verifyAndAuthMessageAPI(string apiData, string xyzChallenge)
        {

        }

        public virtual void verifyAndAuthCompleted(bool isSuccessful, string errorText)
        {

        }

        public virtual void displayGroupList(int reqId, string groups)
        {

        }

        public virtual void displayGroupUpdated(int reqId, string contractInfo)
        {

        }

        public virtual void connectAck()
        {
            Console.WriteLine("CONNECTED!@#");

            if (ClientSocket.AsyncEConnect)
                ClientSocket.startApi();

        }

        public virtual void positionMulti(int requestId, string account, string modelCode, Contract contract, double pos, double avgCost)
        {

        }

        public virtual void positionMultiEnd(int requestId)
        {

        }

        public virtual void accountUpdateMulti(int requestId, string account, string modelCode, string key, string value, string currency)
        {

        }

        public virtual void accountUpdateMultiEnd(int requestId)
        {

        }

        public virtual void securityDefinitionOptionParameter(int reqId, string exchange, int underlyingConId, string tradingClass, string multiplier, HashSet<string> expirations, HashSet<double> strikes)
        {

        }

        public virtual void securityDefinitionOptionParameterEnd(int reqId)
        {

        }

        public virtual void softDollarTiers(int reqId, SoftDollarTier[] tiers)
        {

        }

        public virtual void familyCodes(FamilyCode[] familyCodes)
        {

        }

        public virtual void symbolSamples(int reqId, ContractDescription[] contractDescriptions)
        {

        }

        public virtual void mktDepthExchanges(DepthMktDataDescription[] depthMktDataDescriptions)
        {

        }

        public virtual void tickNews(int tickerId, long timeStamp, string providerCode, string articleId, string headline, string extraData)
        {

        }

        public virtual void smartComponents(int reqId, Dictionary<int, KeyValuePair<string, char>> theMap)
        {

        }

        public virtual void tickReqParams(int tickerId, double minTick, string bboExchange, int snapshotPermissions)
        {
            Console.WriteLine("tickReqParams [" + tickerId + " | " + minTick + " | " + bboExchange + " | " + snapshotPermissions + "]");
        }

        public virtual void newsProviders(NewsProvider[] newsProviders)
        {

        }

        public virtual void newsArticle(int requestId, int articleType, string articleText)
        {

        }

        public virtual void historicalNews(int requestId, string time, string providerCode, string articleId, string headline)
        {

        }

        public virtual void historicalNewsEnd(int requestId, bool hasMore)
        {

        }

        public virtual void headTimestamp(int reqId, string headTimestamp)
        {

        }

        public virtual void histogramData(int reqId, HistogramEntry[] data)
        {

        }

        public virtual void rerouteMktDataReq(int reqId, int conId, string exchange)
        {
            Console.WriteLine("rerouteMktDataReq [" + reqId + " | " + conId + " | " + exchange + "]");
        }

        public virtual void rerouteMktDepthReq(int reqId, int conId, string exchange)
        {
            Console.WriteLine("rerouteMktDepthReq [" + reqId + " | " + conId + " | " + exchange + "]");
        }

        public virtual void marketRule(int marketRuleId, PriceIncrement[] priceIncrements)
        {

        }

        public virtual void pnl(int reqId, double dailyPnL, double unrealizedPnL, double realizedPnL)
        {

        }

        public virtual void pnlSingle(int reqId, int pos, double dailyPnL, double unrealizedPnL, double realizedPnL, double value)
        {

        }

        public virtual void historicalTicks(int reqId, HistoricalTick[] ticks, bool done)
        {

        }

        public virtual void historicalTicksBidAsk(int reqId, HistoricalTickBidAsk[] ticks, bool done)
        {

        }

        public virtual void historicalTicksLast(int reqId, HistoricalTickLast[] ticks, bool done)
        {

        }

        public virtual void tickByTickAllLast(int reqId, int tickType, long time, double price, int size, TickAttribLast tickAttriblast, string exchange, string specialConditions)
        {

        }

        public virtual void tickByTickBidAsk(int reqId, long time, double bidPrice, double askPrice, int bidSize, int askSize, TickAttribBidAsk tickAttribBidAsk)
        {

        }

        public virtual void tickByTickMidPoint(int reqId, long time, double midPoint)
        {

        }

        public virtual void orderBound(long orderId, int apiClientId, int apiOrderId)
        {

        }

        public virtual void completedOrder(Contract contract, Order order, OrderState orderState)
        {

        }

        public virtual void completedOrdersEnd()
        {

        }

        public void error(int id, int errorCode, string errorMsg, string advancedOrderRejectJson)
        {
            Console.WriteLine("ERROR: " + id + "..." + errorCode + "..." + errorMsg);

            if ((errorCode == 1100 && errorMsg.ToLower().Contains("connect")) || (errorCode == 10182 && errorMsg.ToLower().Contains("failed")) || errorMsg.Contains("Request Tick-By-Tick Data Sending Error"))
            {
                if (!sentRestartAlert)
                {
                    //sendAlertAsync("Restarting");
                    sentRestartAlert = true;
                }

                System.Diagnostics.Process.Start("C:\\FuturesSystem\\start.bat");

            }

        }

        public void tickSize(int tickerId, int field, decimal size)
        {
            Console.WriteLine("tickSize: " + tickerId + "..." + field + "..." + size);
        }

        public void tickOptionComputation(int tickerId, int field, int tickAttrib, double impliedVolatility, double delta, double optPrice, double pvDividend, double gamma, double vega, double theta, double undPrice)
        {
            throw new NotImplementedException();
        }

        public void updatePortfolio(Contract contract, decimal position, double marketPrice, double marketValue, double averageCost, double unrealizedPNL, double realizedPNL, string accountName)
        {
            throw new NotImplementedException();
        }


        public void updateMktDepth(int tickerId, int position, int operation, int side, double price, decimal size)
        {
            throw new NotImplementedException();
        }

        public void updateMktDepthL2(int tickerId, int position, string marketMaker, int operation, int side, double price, decimal size, bool isSmartDepth)
        {
            throw new NotImplementedException();
        }

        public virtual void position(string account, Contract contract, decimal pos, double avgCost)
        {
            Console.WriteLine("Position. " + account + " - Symbol: " + contract.Symbol + ", SecType: " + contract.SecType + ", Currency: " + contract.Currency +
                ", Position: " + Util.DecimalMaxString(pos) + ", Avg cost: " + Util.DoubleMaxString(avgCost));
            
            if(pos > 0)
            {
                sendAlertAsync("Started with open position!");
            }

        }

        public void realtimeBar(int reqId, long date, double open, double high, double low, double close, decimal volume, decimal WAP, int count)
        {
            throw new NotImplementedException();
        }

        public void positionMulti(int requestId, string account, string modelCode, Contract contract, decimal pos, double avgCost)
        {
            throw new NotImplementedException();
        }

        public void pnlSingle(int reqId, decimal pos, double dailyPnL, double unrealizedPnL, double realizedPnL, double value)
        {
            throw new NotImplementedException();
        }

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            return epocDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
        }


        public IndexQuote tickToQuote(DateTime theDate, decimal tick)
        {
            IndexQuote q = new IndexQuote();

            q.lastUpdated = DateTime.Now;
            q.Date = theDate;
            q.Open = tick;
            q.High = tick;
            q.Low = tick;
            q.Close = tick;


            return q;

        }

        public void updateQuote(ref IndexQuote q, decimal price)
        {
            if (price > q.High)
                q.High = price;
            else if (price < q.Low || q.Low == 0)
                q.Low = price;

            if (q.Open == 0)
                q.Open = price;

            q.Close = price;

            q.lastUpdated = DateTime.Now;

        }

        public void tickByTickAllLast(int reqId, int tickType, long time, double price, decimal size, TickAttribLast tickAttribLast, string exchange, string specialConditions)
        {
            //TimeSpan them = new TimeSpan(0, barTime.Hour, barTime.Minute, barTime.Second, 0);
            //TimeSpan us = new TimeSpan(0, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond);

            //if (barTime.Minute  >= 5)
            //   Console.WriteLine("new 5 min bar for " + time    + " .... " + DateTime.Now.ToString("hh.mm.ss.ffffff") + " " +     (us - them).TotalMilliseconds);


            DateTime barTime = UnixTimeStampToDateTime(time);

            //Console.WriteLine("Tick-By-Tick. Request Id: {0}, TickType: {1}, Time: {2}, Price: {3}, Size: {4}, Exchange: {5}, Special Conditions: {6}, PastLimit: {7}, Unreported: {8}",
            //        reqId, tickType == 1 ? "Last" : "AllLast", barTime.ToString("yyyyMMdd  HH:mm:ss"), Util.DoubleMaxString(price), Util.DecimalMaxString(size), exchange, specialConditions, tickAttribLast.PastLimit, tickAttribLast.Unreported);


      

            DateTime theDate = barTime.Date;

            int minuteMain = barTime.Minute / 10;
            int minuteRem = barTime.Minute % 10;

            TimeSpan ts;


            if (minuteRem < 5)
            {
                ts = new TimeSpan(barTime.Hour, minuteMain * 10, 0);
            }
            else
                ts = new TimeSpan(barTime.Hour, minuteMain * 10 + 5, 0);

            theDate = theDate + ts;

            Symbol s = activeSymbols[reqId];

            string key = getIndexKey(s.symbol, theDate.ToString("yyyyMMdd  HH:mm:ss"));


            if (rollingData.ContainsKey(key))
            {
                IndexQuote q = rollingData[key];
                updateQuote(ref q, Convert.ToDecimal(price));
                rollingData[key] = q;
              
            }
            else
            {
                Console.WriteLine("Adding from push: " + theDate.ToString("yyyyMMdd  HH:mm:ss"));

                IndexQuote currQuote = tickToQuote(theDate, Convert.ToDecimal(price));
                currQuote.lastUpdated = DateTime.Now;

                rollingData.Add(key, currQuote);
            }

        }

        public void tickByTickBidAsk(int reqId, long time, double bidPrice, double askPrice, decimal bidSize, decimal askSize, TickAttribBidAsk tickAttribBidAsk)
        {
            throw new NotImplementedException();
        }

        public void replaceFAEnd(int reqId, string text)
        {
            throw new NotImplementedException();
        }

        public void wshMetaData(int reqId, string dataJson)
        {
            throw new NotImplementedException();
        }

        public void wshEventData(int reqId, string dataJson)
        {
            throw new NotImplementedException();
        }

        public void historicalSchedule(int reqId, string startDateTime, string endDateTime, string timeZone, HistoricalSession[] sessions)
        {
            throw new NotImplementedException();
        }

        public void userInfo(int reqId, string whiteBrandingId)
        {
            throw new NotImplementedException();
        }

        public void accountSummary(int reqId, string account, string tag, string value, string currency)
        {
            //Console.WriteLine("Acct Summary. ReqId: " + reqId + ", Acct: " + account + ", Tag: " + tag + ", Value: " + value + ", Currency: " + currency);
        }

        public void accountSummaryEnd(int reqId)
        {
            Console.WriteLine("accountSummaryEnd: " + reqId);
        }
    }
}
