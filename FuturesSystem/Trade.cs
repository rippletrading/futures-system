using IBApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuturesSystem
{
  public class Trade
  {
    public decimal filledVolume;
    public decimal avgFillPrice;
    public decimal requestedVolume;
    public int orderID;
    public OrderState orderState;
    public string orderStatus;
    public Future future;
    public FuturesEnums.OPEN_OR_CLOSE openOrClose;


  }
}
